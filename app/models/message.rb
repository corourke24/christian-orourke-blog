class Message < ApplicationRecord
    include ActiveModel::Model
    attr_accessor :name, :email, :content
    validates :content, presence: true
    validates :name,  presence: true, length: { maximum: 50 }
    EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 245 },
                      format: { with: EMAIL_REGEX }
end

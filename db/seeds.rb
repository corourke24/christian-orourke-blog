User.create!(name:  "Christian",
             email: "moonhalf@gmail.com",
             password:              "Moon100HalfWebDev",
             password_confirmation: "Moon100HalfWebDev",
             id: 1,
             admin: true)

Post.create!(title: 'Moon Half',
            body: "Moon Half is an online blog for sale with some great visual effects! The blog is fully functioning and scales across a wide range of screen sizes.
            Once purchased, all graphics and working can be changed to create a unique blogging experience. If you're interested in buying this blog, or have any questions, 
            please feel free to contact me",
            description: "Moon Half Visual Blog",
            category_id: 1)
            
Category.create!(name: "Uncategorized",
                id: 1)
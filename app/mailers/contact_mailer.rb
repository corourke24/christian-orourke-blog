class ContactMailer < ApplicationMailer
    def contact(message) 
       @content = message.content
       
       mail to: "christian.c.orourke@gmail.com", from: message.email
    end
end
